function testFlatten(elements) {
    if (elements) {
        let temp = elements;
        const result = [];
        while (temp.length) {
            subArray = temp.shift();
            if (Array.isArray(subArray)) {
                temp = temp.concat(subArray);
            } else {
                result.push(subArray);
            }
        }
        return result;
    } else {
        return undefined;
    }
}

//console.log(testFlatten([1, [2], [[3]], [[[4]]]]));
module.exports = testFlatten;
