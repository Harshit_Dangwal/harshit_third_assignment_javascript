function testEach(elements,cb) {
    if (elements) {
        for ( let index = 0; index < elements.length; index++) {
            cb(elements[index], index, elements)
        }
    }else{
        return undefined;
    }

} 

testEach([1, 2, 3],cb=(index,value,elements)=>console.log(`index:${index}, value:${value}, elements:${elements}`));
module.exports = testEach;