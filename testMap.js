function testMap(elements,cb) {
    if (elements) {
        const result = [];
        for (let index = 0; index < elements.length; index++) {
            result.push(cb(elements[index]));
        }
        return result;
    }else{
        return [];
    }
}
console.log(testMap([1, 2, 3, 4, 5, 5],cb=(a)=>a**2));
module.exports = testMap;