function testReduce(elements, startingValue, cb) {
    if (elements) {
        if (startingValue || startingValue === 0) {
            sum = startingValue;
            for (let index = 0; index < elements.length; index++) {
                sum = cb(sum, elements[index]);
            }
            return sum;
        } else {
            sum = elements[0];
            for (let index = 0; index < elements.length; index++) {
                sum = cb(sum, elements[index]);
            }
            return sum;
        }
    }
}

console.log(testReduce([1, 2, 3], 0,cb = (sum, value) => sum += value));

module.exports = testReduce;