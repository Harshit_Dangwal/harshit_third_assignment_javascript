function testFilter(elements,cb) {
    if (elements) {
        const result = []
        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index])) {
                result.push(elements[index],index,elements);
            }
        }
        return result;
    } else {
        return undefined;
    }
}
console.log(testFilter([1, 2, 20, 4, 19, 5],cb=(testAge)=>testAge>18));

module.exports = testFilter;