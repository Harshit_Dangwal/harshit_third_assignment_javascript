const testFlatten = require("../testFlatten")

console.log(testFlatten([[[1, [2], [3, [[4]]]]]]));
// [ 1, 2, 3, 4 ]
console.log(testFlatten([]));
// []
console.log(testFlatten(undefined));
// undefined