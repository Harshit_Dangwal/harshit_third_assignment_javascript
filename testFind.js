function testFind(elements,cb) {
    if (elements) {
        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index],index,elements)) {
                return elements[index] +", index:"+ index+", elements:"+elements;
            }
        }
        return undefined;
    }
}
console.log(testFind([1, 19, 3, 4, 5, 5],cb=(value)=>value>18));
module.exports = testFind;